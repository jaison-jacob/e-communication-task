import * as Yup from "yup";
import { string } from "yup/lib/locale";
export const initialValue = {
  personalDetails: {
    mailId: "",
    mobNo: "",
    name: "",
    age: "",
    genderId: 1,
    dateOfBirth: null,
    motherTongueId: null,
    preferredLanguageId: [],
    knownViaProducts: [],
    others: "",
  },
  addressDetails: {
    address: "",
    stateId: null,
    districtId: null,
    pincode: "",
    country: "",
    type: null,
  },
  qualificationDetails: {
    userRoleId: 1,
    userQualificationId: null,
    institutionName: null,
    institutionAddress: null,
    country: "",
    studyingAt: "",
    stateId: null,
    districtId: null,
    pincode: null,
    levelId: null,
    annumSal: null,
  },
  credentialDetails: {
    userName: "",
    generatePassword: "",
  },
};

export const individualUserFormValidation = Yup.object({
  formControl: Yup.string(),
  personalDetails: Yup.mixed().when("formControl", {
    is: "personalDetails",
    then: Yup.object({
      name: Yup.string().required("required").min(5, "writw 3"),
      age: Yup.number().required("required").min(10, "age should be above 10"),
      mailId: Yup.string()
        .email("please enter valid email id")
        .required("required"),
      mobNo: Yup.string().min(10, "please enter valid phone number"),
      dateOfBirth: Yup.string().required("required").nullable(true),
      preferredLanguageId: Yup.array().required("requir"),
    }),
  }),
  addressDetails: Yup.mixed().when("formControl", {
    is: "addressDetails",
    then: Yup.object({
      address: Yup.string()
        .required("required")
        .min(10, "please enter above 10"),

      stateId: Yup.string().required("required"),
      country: Yup.string().required("required"),

      districtId: Yup.mixed().when("country", {
        is: 1,
        then: Yup.number().required("district is required"),
      }),
      pincode: Yup.mixed().when("country", {
        is: 1,
        then: Yup.number().required("pincode is required"),
      }),
    }),
    qualificationDetails: Yup.mixed().when("formControl", {
      is: "qualificationDetails",
      then: Yup.object({
        institutionName: Yup.string()
          .min(10, "Must be exactly 10 digits")
          .required("required"),
        institutionAddress: Yup.string()
          .min(10, "Must be exactly 10 digits")
          .required("reequired"),
      }),
    }),
  }),

  // personalDetails:Yup.object({
  // 	name:Yup.string().min(5, 'Must be exactly 5 digits'),
  // 	mailId:Yup.string().email("please enter valid email id"),
  // 	mobNo:Yup.string()
  // 	.min(10,"please enter valid phone number"),
  // 	age:Yup.number().min(10,"age should be above 10"),
  // }),
  addressDetails: Yup.object({
    address: Yup.string().min(10, "Must be exactly 10 digits"),
    // 	stateId:Yup.string()
    // .when('country', {
    // 	is: 1,
    // 	then: Yup.string()
    // 	  .required('state is required').nullable(true),
    //   }),
    // districtId:Yup.string().required("required"),
    // pincode:Yup.string().required("required"),
    // country:Yup.string().required("required"),
    // type:Yup.string().required("required"),
  }),
  qualificationDetails: Yup.object({
    institutionName: Yup.string().min(10, "Must be exactly 10 digits"),
    institutionAddress: Yup.string().min(10, "Must be exactly 10 digits"),
  }),
});
