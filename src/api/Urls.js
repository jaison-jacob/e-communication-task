export const Url = {
    GET_KNOWINGVIAPRODUCT: "knownviaproducts",
    GET_GENDER: "gender",
    GET_LANGUAGE: "languages",
    GET_STATES:"states",
    GET_DISTRICT:"districts/",
    GET_USERROLES:"userRoles",
    GET_CURRENTQUALIFICATION:"qualification",
    GET_NOTIFICATIONDETAILSBYID:"getDetailById/notifications/",
    GET_PROFESSIONALLEVEL:"professionalLevel",
    GET_ANNUMSALERY:"annumSalary",
    POST_INDIVIDUALUSER:"createUsers",
    GET_ALLINIDIVIDUALUSERS:"allUsers",

}