import axios from "axios";
import { Url } from "./Urls";

export const appApi = axios.create({
  baseURL: "http://localhost:4000/",
  // headers: { Authorization: `Bearer` },
});

export const getLanguages = () => appApi.get(Url.GET_LANGUAGE);
export const getGender = () => appApi.get(Url.GET_GENDER);
export const getKnowingViaProduct = () => appApi.get(Url.GET_KNOWINGVIAPRODUCT);
export const getState = () => appApi.get(Url.GET_STATES);
export const getDistrict = (id) => appApi.get(Url.GET_DISTRICT +`${id}`);
export const getUserRoles = () => appApi.get(Url.GET_USERROLES );
export const getCurrentQualification = () => appApi.get(Url.GET_CURRENTQUALIFICATION );
export const getProffesionalLevel = () => appApi.get(Url.GET_PROFESSIONALLEVEL );
export const getAnnumSalery = () => appApi.get(Url.GET_ANNUMSALERY );
// export const getNotificationData = (from,to) =>
//   appApi.post(Url.GET_NOTIFICATIONDETAILS+`${to}/${from}`);
  export const getCurrentQualificatio = (from,to) =>
  appApi.post(Url.GET_NOTIFICATIONDETAILS+`${to}/${from}`);
export const getAllIndividualUser = () =>
  appApi.get(Url.GET_ALLINIDIVIDUALUSERS);

export const postIndividualUser = (data) =>
  appApi.post(Url.POST_INDIVIDUALUSER, data);
export const UpdateNotificationType = (data, id) =>
  appApi.put(Url.UPDATE_NPTIFICATION + `${id}`, data);
