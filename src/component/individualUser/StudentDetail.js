import { Button, Grid, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useStyles } from "../styles/PersonalDetail";
import {
  InputField,
  SelectField,
  AutoCompleteField,
  CheckBoxFeild,
  FormWrapper,
  SingleCheckBoxFeild
} from "../reusables";
import { useFormikContext } from "formik";
import { getState, getDistrict,getCurrentQualification } from "../../api/Api";
import { checkedState,initialValue } from "../../constants/induvidualUser";

function StudentDetail(props) {
  const Styles = useStyles();
  const [stateOption, setstateOption] = useState([]);
  const [districtOption, setdistrictOption] = useState([]);
  const [currentQualification, setcurrentQualification] = useState([]);

  const { submitForm,setFieldValue, errors, values } = useFormikContext();


  useEffect(() => {
    getState().then((response) => {
      setstateOption(response.data);
    });
    getCurrentQualification().then((response) => {
        setcurrentQualification(response.data)
    });
    
  }, []);

  useEffect(() => {
    if(values.qualificationDetails.stateId !== null && typeof values.qualificationDetails.stateId !== "string"){
      getDistrict(values.qualificationDetails.stateId).then((response) => {
        setdistrictOption(response.data);
      });
    }
  }, [values.qualificationDetails.stateId]);


  return (
    <div>
    <FormWrapper>
      <Grid container spacing={3}>
        <Grid item xs={12}>
        <SelectField
                variant="filled"
                label="Current Qualification"
                selectLabelStyle={Styles.selectLabel}
                name="qualificationDetails.userQualificationId"
                color="primary"
                selectOptionStyle={Styles.selectOption}
                options={currentQualification}
              />
        </Grid>
        <Grid item xs={6}>
            <InputField
              name="qualificationDetails.institutionName"
              label="Institution Name"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
            />
          </Grid>
          <Grid item xs={6}>
            <InputField
              name="qualificationDetails.studyingAt"
              label="Studying At"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
            />
          </Grid>
          <Grid item xs={12}>
            <InputField
              name="qualificationDetails.institutionAddress"
              label="Institution Address"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
            />
          </Grid>
        <Grid item xs={6}>
          <SelectField
            variant="filled"
            label="Country"
            selectLabelStyle={Styles.selectLabel}
            name="qualificationDetails.country"
            color="primary"
            selectOptionStyle={Styles.selectOption}
            options={[
              { id: 1, name: "India" },
              { id: 2, name: "Oversease" },
            ]}
          />
        </Grid>
        {values.qualificationDetails.country === 1 ? (
          <>
            <Grid item xs={6}>
              <SelectField
                variant="filled"
                label="State"
                selectLabelStyle={Styles.selectLabel}
                name="qualificationDetails.stateId"
                color="primary"
                selectOptionStyle={Styles.selectOption}
                options={stateOption}
              />
            </Grid>
            <Grid item xs={6}>
              <SelectField
                variant="filled"
                label="District"
                selectLabelStyle={Styles.selectLabel}
                name="qualificationDetails.districtId"
                color="primary"
                selectOptionStyle={Styles.selectOption}
                options={districtOption}
              />
            </Grid>
            <Grid item xs={6}>
              <InputField
                name="qualificationDetails.pincode"
                label="Pincode"
                type="Number"
                fullWidth={true}
                variant="filled"
                color="primary"
                textStyle={Styles.textField}
              />
            </Grid>
          </>
        ) : (
          <Grid item xs={6}>
            <InputField
              name="qualificationDetails.stateId"
              label="State"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
            />
          </Grid>
        )}
        
      </Grid>
    </FormWrapper>
    </div>
  );
}

export default StudentDetail;
