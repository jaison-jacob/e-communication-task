import { Button, Grid, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useStyles } from "../styles/PersonalDetail";
import {
  InputField,
  SelectField,
  AutoCompleteField,
  CheckBoxFeild,
  FormWrapper,
  SingleCheckBoxFeild,
  RadioField
} from "../reusables";
import { Field, useFormikContext } from "formik";
import { getState, getDistrict,getUserRoles } from "../../api/Api";
import { checkedState } from "../../constants/induvidualUser";
import StudentDetail from "../../component/individualUser/StudentDetail";
import ProfessionalForm from "../../component/individualUser/ProfessionalForm";
import Housewifes from "../../component/individualUser/Housewifes";
import {initialValue} from "../../constants/induvidualUser"

function ProfessionalDetails(props) {
  const Styles = useStyles();
  const [userRoles, setuserRoles] = useState([]);

  const { values } = useFormikContext();
const {setFieldValue} = useFormikContext()

  useEffect(() => {
    getUserRoles().then((response) => {
        setuserRoles(response.data)
    })
  }, []);

let comp = [<StudentDetail/>,<ProfessionalForm/>,<Housewifes/>]
const getComp = (ItemId) => {
    return comp[ItemId]
}
  return (
    <div>
    <FormWrapper>
      
      <Grid container spacing={3}>
      <Grid item xs={12}>
        
          <RadioField
            labelClasses={Styles.radioHead}
            name="qualificationDetails.userRoleId"
            options={userRoles}
            radioLabel={{
              label: Styles.radioLabel,
            }}
            boxColor={{color:"black"}}
          />
       </Grid>
       </Grid>
    </FormWrapper>
    <div style={{marginTop:20}}>
   {
       getComp(values.qualificationDetails.userRoleId -1)
   }
    </div>
    </div>
  );
}

export default ProfessionalDetails;
