import { Button, Grid, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useStyles } from "../styles/PersonalDetail";
import {
  InputField,
  SelectField,
  AutoCompleteField,
  CheckBoxFeild,
  FormWrapper,
  SingleCheckBoxFeild
} from "../reusables";
import { useFormikContext } from "formik";
import { getAnnumSalery,getProffesionalLevel } from "../../api/Api";
import { checkedState } from "../../constants/induvidualUser";

function ProfessionalForm(props) {
  const Styles = useStyles();
  const [professioalLevel, setprofessioalLevel] = useState([]);
  const [annumSalary, setannumSalary] = useState([]);
 

  const { submitForm, errors, values } = useFormikContext();

  

  useEffect(() => {
    getProffesionalLevel().then((response) => {
      setprofessioalLevel(response.data);
    });
    getAnnumSalery().then((response) => {
        setannumSalary(response.data)
    });
  //   let activeComponent = { ...initialValue["qualificationDetails"] };
  // setFieldValue("qualificationDetails", activeComponent);
  }, []);

  


  return (
    <div>
    <FormWrapper>
      <Grid container spacing={3}>
        <Grid item xs={12}>
        <SelectField
                variant="filled"
                label="Level"
                selectLabelStyle={Styles.selectLabel}
                name="qualificationDetails.levelId"
                color="primary"
                selectOptionStyle={Styles.selectOption}
                options={professioalLevel}
              />
        </Grid>
        <Grid item xs={6}>
        <SelectField
                variant="filled"
                label="Level"
                selectLabelStyle={Styles.selectLabel}
                name="qualificationDetails.annumSal"
                color="primary"
                selectOptionStyle={Styles.selectOption}
                options={annumSalary}
              />
        </Grid>
        </Grid>
       
    </FormWrapper>
    </div>
  );
}

export default ProfessionalForm;
