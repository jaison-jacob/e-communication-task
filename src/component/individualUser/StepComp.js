import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import {RadioButtonChecked} from '@material-ui/icons';
import SettingsIcon from '@material-ui/icons/Settings';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import VideoLabelIcon from '@material-ui/icons/VideoLabel';
import StepConnector from '@material-ui/core/StepConnector';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CheckIcon from '@material-ui/icons/Check';

export  const QontoConnector = withStyles({
  vertical: {
    // top: 10,
    // left: 'calc(-50% + 16px)',
    // right: 'calc(50% + 16px)',
  },
  active: {
    '& $lineVertical': {
      borderColor: '#2BAA96',
    },
  },
  completed: {
    '& $lineVertical': {
      borderColor: '#2BAA96',
    },
  },
  lineVertical: {
    borderColor: '#707070',
    borderLeftWidth: 2,
    borderRadius: 1,
    padding: "8px 0px",
    marginLeft: 14
  },
})(StepConnector);

const useQontoStepIconStyles = makeStyles({
  root: {
    color: '#eaeaf0',
    
    display: 'flex',
    height: 22,
    alignItems: 'center',
  },
  active: {
    width: 26,
    height: 26,
    borderRadius: '50%',
    color: '#FFFFFF',
  
    border:"2px solid #2BAA96",
   backgroundColor:"#2BAA96",
   display:"flex",
   justifyContent:"center",
   alignItems:"center"
  },
  circle: {
    width: 26,
    height: 26,
    borderRadius: '50%',
    border:"2px solid #707070",
    color:"#707070",
    backgroundColor:"#FFFFFF",
  
   display:"flex",
   justifyContent:"center",
   alignItems:"center"
  },
  completed: {
    color: '#FFFFFF',
    zIndex: 1,
    // fontSize: 35,
    // marginRight: 10
  },
  completeColor:{
    backgroundColor:"#2BAA96",
    border:"2px solid #2BAA96",
  }
});

export function StepIcon(props) {
  
  const classes = useQontoStepIconStyles();
  const { active, completed,icon } = props;

  return (
    <div
      className={clsx(classes.root)}
    >
    { <div className={clsx({[classes.circle]:!active, 
        [classes.active]: active,
        [classes.completeColor]:completed
      })}>{

completed ?
<CheckIcon className={classes.completed}/> :
icon

      }</div>}
    </div>
  );
}