import React from "react";
import { makeStyles,styled } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepContent from "@material-ui/core/StepContent";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import { FormWrapper } from "../../component/reusables";
import { QontoConnector, StepIcon } from "../individualUser/StepComp";
import { StepConnector } from "@material-ui/core";
import { styles } from "@material-ui/pickers/views/Calendar/Calendar";

const useStyles = makeStyles((theme) => ({
  root: {
    // width: "100%",
    '& .MuiStepConnector-active': { 
      borderColor: "green"
    },
  },
  iconS: {
    borderLeftWidth: 2,
    borderColor: "#707070",
  },
  vAlign: {
    marginLeft: theme.spacing(1.8),
    marginTop: theme.spacing(0.5),
  },
  label: {
    color: "green",
    // backgroundColor:"green"
  },
  act:{
    // color:"blue",
    borderColor:"green"
  },
  lineVertical: {
    padding: "8px 0px",
    marginLeft: 14,
    borderLeftWidth:3
  },
  line:{
    borderLeftWidth:2,
    borderColor:"#707070",
  },
  connectorActive: {
    '& .MuiStepConnector-active': {
      borderColor: "green"
    }
  },
}));  

const StyledStepLabel = styled(StepLabel)({
  "& .MuiStepLabel-label":{
    color:"#0000008A",
    fontSize:14,
    fontFamily:"Roboto",
    
  },
  "& .MuiStepLabel-active": {
    color: "#2BAA96",
    fontWeight:600
  },
  "& .MuiStepLabel-completed":{
    color:"#2BAA96",
    fontWeight:600
  }
 
});


export default function StepperDemo(props) {
  const classes = useStyles();
  const { activeStep,completed, setActiveStep, steps } = props;

  const handleNext = (index) => {
    setActiveStep(index);
  };

  
  const connector = (
    <StepConnector
    className={classes.root}
      classes={{
        vertical: classes.lineVertical,
        lineVertical: classes.line,
        active: classes.connectorActive,
        completed: classes.connectorActive
      }}
    />
  );


const isCompleted = (index) => {
  return completed.has(index)
}

  return (
    <div className={classes.root}>
      <FormWrapper>
        <Stepper
          activeStep={activeStep}
          connector={connector}
          orientation="vertical"
          style={{backgroundColor:"#F1F1F1"}}
        >
          {steps.map((item, index) => {
            const stepProps = {};
            const labelProps = {};

            if(isCompleted(index)){
              stepProps.completed = true
              stepProps.active = false
            }else{
              if(index <= activeStep){
                stepProps.active = true
                stepProps.completed = false
              }
            }
            

            
            return (
<Step key={index}
{...stepProps}
            >
              <StyledStepLabel
                onClick={() => handleNext(index)}
               StepIconComponent={StepIcon}
              >
                {item.label}
              </StyledStepLabel>
            </Step>
            )
            
})}
        </Stepper>
      </FormWrapper>
    </div>
  );
}
