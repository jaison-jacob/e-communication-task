import { Button, Grid, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useStyles } from "../styles/PersonalDetail";
import {
  InputField,
  FormWrapper,
  ButtonWrapper,
  Modal,
  Alert,
} from "../reusables";
import { v4 as uuid } from "uuid";
import { useFormikContext } from "formik";

function CredentialDetail(props) {
  const Styles = useStyles();
  const [Gpassword, setPassword] = useState("");
  const { setFieldValue, values } = useFormikContext();

  const [open, setOpen] = React.useState(false);
  const [modalComp, setModalComp] = useState();

  const generatePassword = () => {

    if (values.credentialDetails.userName.length > 3) {
      let password = uuid();
      setFieldValue("credentialDetails.generatePassword", password);
      setPassword(password);
    } else {
      setOpen(true);
      setModalComp(
        <Alert
          yesFun={() => okFun()}
          alertMsg=" please enter user name and user name should be 3 charecter "
          NoBtn={false}
          BtnText="Ok"
        />
      );
    }
  };

  const sendMail = () => {
    if (Gpassword.length > 0) {
      setOpen(true);
      setModalComp(
        <Alert
          yesFun={() => okFun()}
          alertMsg=" send mail successfully "
          NoBtn={false}
          BtnText="Ok"
        />
      );
    }
  };
  const okFun = () => {
    setOpen(false);
  };
 
  return (
    <div>
      <FormWrapper>
        <Grid container spacing={3}>
          <Grid item xs={6}>
            <InputField
              name="credentialDetails.userName"
              label="User name"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
            />
          </Grid>
          <Grid item xs={6}></Grid>
          <Grid item xs={5}>
            {values.credentialDetails.generatePassword.length === 0 ? (
              <ButtonWrapper
                text="Generate Password"
                btnStyle={Styles.sendEmail}
                fullWidth={true}
                size="medium"
                variant="filled"
                BtnAction={() => generatePassword()}
              />
            ) : (
              <input
                type="text"
                name="credentialDetails.generatePassword"
                placeholder="Generate Password"
                style={{
                  height: 36,
                  borderRadius: 2,
                  backgroundColor: "#0000000A",
                  color: "#0000008A",
                  fontFamily: "Rubik",
                  fontSize: 16,
                  textAlign: "center",
                  border: "none",
                }}
                value={Gpassword || ""}
                disabled
              />
            )}
            <>{/* */}</>
          </Grid>
          <Grid item xs={6}>
            <ButtonWrapper
              text="Send Mail"
              btnStyle={Styles.sendEmail}
              size="medium"
              variant="filled"
              BtnAction={() => sendMail()}
            />
          </Grid>
        </Grid>
      </FormWrapper>
      <Modal open={open} setOpen={setOpen}>
        {modalComp}
      </Modal>
    </div>
  );
}

export default CredentialDetail;
