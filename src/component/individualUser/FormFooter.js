import React, { useEffect, useRef, useState } from "react";
import { useStyles } from "../styles/FormFooter";
import { ButtonWrapper } from "../reusables";
import { useFormikContext } from "formik";
import { initialValue } from "../../constants/induvidualUser";
import { Modal, Alert } from "../reusables";

function FormFooter(props) {
  const styles = useStyles();

  const {
    activeStep,
    skippedStep,
    completed,
    setActiveStep,
    setSkippedStep,
    setCompleted,
    steps,
    open,
    setModalComp,
    setOpen,
  } = props;
  // console.log(skippedStep)
  const {
    values,
    setFieldValue,
    submitForm,
    setFieldError,
    touched,
    setTouched,
    errors,
    isSubmitting,
    setSubmitting,
  } = useFormikContext();
  // console.log(useFormikContext())

 

  const skipForm = () => {
   

    if (Object.keys(touched).length !== 0) {
      setOpen(true);
      setModalComp(
        <Alert
          noFun={() => noFun()}
          yesFun={() => yesFun()}
          alertMsg=" If You Skip This Form Filled Data Will Lose "
          conditionMsg="Do You Want To Skip"
          NoBtn={true}
          BtnText="Yes"
        />
      );
    } else {
      skippedStep.push(activeStep);
      setSkippedStep(skippedStep);
      setActiveStep(activeStep + 1);
    }
  };
  const submitForms = () => {
    if (activeStep === steps.length - 1) {
      if (completed.size > 0) {
        submitForm();
      } else {
        setOpen(true);
        setModalComp(
          <Alert
            yesFun={() => okFun()}
            alertMsg=" please filled any one of the form "
            NoBtn={false}
            BtnText="Ok"
          />
        );
      }
    } else {
      submitForm();
    }
  };

  useEffect(() => {
    setTouched({});
    setFieldValue("formControl", steps[activeStep].formName);
  }, [activeStep]);


  const yesFun = () => {
    let activeComponent = { ...initialValue[steps[activeStep].formName] };
    setFieldValue(steps[activeStep].formName, activeComponent);
    skippedStep.push(activeStep);
    setSkippedStep(skippedStep);
    setActiveStep(activeStep + 1);
    setOpen(false);
  };
  const noFun = () => {
    setOpen(false);
  };
  const okFun = () => {
    setOpen(false);
  };

 
  return (
    <div className={styles.footerContainer}>
      <div className={styles.buttonContainer}>
        <ButtonWrapper
          text="Cancel"
          btnStyle={styles.cancelBtn}
          size="medium"
          variant="outlined"
        />
        <ButtonWrapper
          text="Skip"
          btnStyle={styles.cancelBtn}
          size="medium"
          variant="outlined"
          BtnAction={() => skipForm()}
          disabled={activeStep === steps.length - 1}
        />
        <ButtonWrapper
          text="Save"
          btnStyle={styles.saveBtn}
          size="medium"
          variant="contained"
          BtnAction={() => submitForms()}
          disabled={Object.keys(touched).length === 0 && activeStep !== steps.length - 1}
        />

        
      </div>
    </div>
  );
}

export default FormFooter;
