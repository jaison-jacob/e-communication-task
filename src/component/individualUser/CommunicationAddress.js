import { Button, Grid, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useStyles } from "../styles/PersonalDetail";
import {
  InputField,
  SelectField,
  AutoCompleteField,
  CheckBoxFeild,
  FormWrapper,
  SingleCheckBoxFeild
} from "../reusables";
import { useFormikContext } from "formik";
import { getState, getDistrict } from "../../api/Api";
import { checkedState } from "../../constants/induvidualUser";

function CommunicationAddress(props) {
  const Styles = useStyles();
  const [stateOption, setstateOption] = useState([]);
  const [districtOption, setdistrictOption] = useState([]);
  const [knowingViaProduct, setKnowingViaProduct] = useState([]);

  const { values } = useFormikContext();

  

  useEffect(() => {
    getState().then((response) => {
      setstateOption(response.data);
    });
  }, []);

  useEffect(() => {
    if(values.addressDetails.stateId !== null && typeof values.addressDetails.stateId !== "string"){
      getDistrict(values.addressDetails.stateId).then((response) => {
        setdistrictOption(response.data);
      });
    }
    
  }, [values.addressDetails.stateId]);

  return (
    <div>
    <FormWrapper>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Typography>Communication Address</Typography>
        </Grid>
        <Grid item xs={12}>
          {/* <InputField
            name="addressDetails.address"
            label="Address"
            fullWidth={true}
            variant="filled"
            color="primary"
            textStyle={Styles.textField}
          /> */}
        </Grid>
        <Grid item xs={6}>
          <SelectField
            variant="filled"
            label="Country"
            selectLabelStyle={Styles.selectLabel}
            name="addressDetails.country"
            color="primary"
            selectOptionStyle={Styles.selectOption}
            options={[
              { id: 1, name: "India" },
              { id: 2, name: "Oversease" },
            ]}
          />
        </Grid>
        {values.addressDetails.country === 1 ? (
          <>
            <Grid item xs={6}>
              <SelectField
                variant="filled"
                label="State"
                selectLabelStyle={Styles.selectLabel}
                name="addressDetails.stateId"
                color="primary"
                selectOptionStyle={Styles.selectOption}
                options={stateOption}
              />
            </Grid>
            <Grid item xs={6}>
              <SelectField
                variant="filled"
                label="District"
                selectLabelStyle={Styles.selectLabel}
                name="addressDetails.districtId"
                color="primary"
                selectOptionStyle={Styles.selectOption}
                options={districtOption}
              />
            </Grid>
            <Grid item xs={6}>
              <InputField
                name="addressDetails.pincode"
                label="Pincode"
                type="Number"
                fullWidth={true}
                variant="filled"
                color="primary"
                textStyle={Styles.textField}
              />
            </Grid>
          </>
        ) : (
          <Grid item xs={6}>
            <InputField
              name="addressDetails.stateId"
              label="State"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
            />
          </Grid>
        )}
        <Grid item xs={12}>
          <SingleCheckBoxFeild
           checkBoxHead={{ root: Styles.radioHead }}
           label="How You come to know about the product"
           checkLabel={{ label: Styles.checkLabel }}
           name="addressDetails.type"
           boxColor={{color:"#2BAA96"}}
          />
        </Grid>
      </Grid>
    </FormWrapper>
    </div>
  );
}

export default CommunicationAddress;
