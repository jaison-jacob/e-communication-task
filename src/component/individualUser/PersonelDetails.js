import { Button, Grid, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useStyles } from "../styles/PersonalDetail";
import {
  InputField,
  RadioField,
  DatePickerField,
  SelectField,
  AutoCompleteField,
  CheckBoxFeild,
  FormWrapper
} from "../reusables";
import { useFormikContext } from "formik";
import { getGender, getLanguages, getKnowingViaProduct } from "../../api/Api";
import {checkedState} from "../../constants/induvidualUser"

function PersonelDetails(props) {
  const Styles = useStyles();
  const [genderOption, setGenderOption] = useState([]);
  const [languageOption, setLanguageOption] = useState([]);
  const [knowingViaProduct, setKnowingViaProduct] = useState([]);

  const { submitForm, errors,values } = useFormikContext();

 

  useEffect(() => {
    getGender().then((response) => {
      setGenderOption(response.data);
    });
    getLanguages().then((response) => {
      setLanguageOption(response.data);
    });
    getKnowingViaProduct().then((response) => {
      setKnowingViaProduct(response.data);
    });
  }, []);

  return (
    <div>
    <FormWrapper>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <InputField
            name="personalDetails.name"
            label="User name"
            fullWidth={true}
            variant="filled"
            color="primary"
            textStyle={Styles.textField}
          />
        </Grid>
        <Grid item xs={6}>
          <RadioField
            labelClasses={Styles.radioHead}
            name="personalDetails.genderId"
            label="Gender"
            options={genderOption}
            radioLabel={{
              label: Styles.radioLabel,
            }}
            // boxColor={{color:"black"}}
          />
        </Grid>
        <Grid item xs={6}>
          <DatePickerField
            autoOk={true}
            inputVariant="filled"
            fullWidth={true}
            label="Date Of Birth"
            name="personalDetails.dateOfBirth"
            format="yyyy/MM/dd"
            minDate={new Date("1990-01-01")}
            maxDate={new Date("2005-01-01")}
          />
        </Grid>
        <Grid item xs={6}>
          <InputField
            name="personalDetails.age"
            label="Age"
            type="Number"
            fullWidth={true}
            variant="filled"
            color="primary"
            textStyle={Styles.textField}
          />
        </Grid>
        <Grid item xs={6}>
          <InputField
            name="personalDetails.mailId"
            label="Mail Id"
            type="email"
            fullWidth={true}
            variant="filled"
            color="primary"
            textStyle={Styles.textField}
          />
        </Grid>
        <Grid item xs={6}>
          <InputField
            name="personalDetails.mobNo"
            label="Mobile No"
            type="Number"
            fullWidth={true}
            variant="filled"
            color="primary"
            textStyle={Styles.textField}
          />
        </Grid>
        <Grid item xs={6}>
          <SelectField
            variant="filled"
            label="product Name"
            selectLabelStyle={Styles.selectLabel}
            name="personalDetails.motherTongueId"
            color="primary"
            selectOptionStyle={Styles.selectOption}
            options={languageOption}
          />
        </Grid>
        <Grid item xs={12}>
          <AutoCompleteField
            options={languageOption}
            style={{ outlined: Styles.chip }}
            chipVariant="outlined"
            textVariant="filled"
            label="Preffered Language For App"
            name="personalDetails.preferredLanguageId"
          />
        </Grid>
        <Grid item xs={12}>
          <CheckBoxFeild
            checkBoxHead={{ root: Styles.radioHead }}
            label="How You come to know about the product"
            options={knowingViaProduct}
            checkLabel={{ label: Styles.checkLabel }}
            name="personalDetails.knownViaProducts"
            boxColor={{color:"black"}}
          />
        </Grid>
        {
          (checkedState(null,"others",values.personalDetails.knownViaProducts)) &&(
            <Grid item xs={6}>
          <InputField
            name="personalDetails.others"
            label="others"
            fullWidth={true}
            variant="filled"
            color="primary"
            textStyle={Styles.textField}
          />
            </Grid>
          )
        }
       

       
      </Grid>
      
    </FormWrapper>
    </div>
  );
}

export default PersonelDetails;
