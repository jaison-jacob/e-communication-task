import React, { useState, useEffect } from "react";
import { Table } from "react_custom_table";
import "react_custom_table/src/styles/index.scss";
import { ArrowDropUp, ArrowDropDown } from "@material-ui/icons";
import { COLUMNS } from "../../../constants/IndividualUserColumn";
import { getAllIndividualUser } from "../../../api/Api";
import { makeStyles } from "@material-ui/core";


export const useStyles = makeStyles((theme) => ({
  form:{
    backgroundColor:"red"
  }
}));


function IndividualUserList() {
  const styles = useStyles()
  console.log("call")
  const [state, setState] = useState({
    snackbarData: { open: false, message: "", severity: "" },
    list: [],
    // modalOpen: false,
    // selectedTheme: {},
    pagination: {
      pageSize: 10,
      currentPage: 1,
    },
    count: 0,
    search: "",
    filterData: {},
    currentSortIndex: null,
    isSorted: false,
  });

  useEffect(() => {
    getAllIndividualUser()
      .then((res) => {
        console.log(res);
        setState({...state,list:[...res.data],count:res.data.length})
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  console.log(state.list)
  
  return (
    <div>
      {state.list.length > 0 && (
        <Table
          columns={COLUMNS  ()}
          data={state.list}
          selection={[]}
          selecte
          LoadingComponent={() => <div></div>}
          tableControlProps={{
            tableTitle: "",
            search: true,
            filter: true,
            addNew: true,
            download: true,
            selectedBackground:"green", 
              searchProps: {
                placeHolder: "his",
                
                onSearchChange: () => console.log("hello"),
              },
          }}
          emptyDataMessage={{
            heading: "No data",
            body: "There seems to be no data found",
          }}
          sortIconTop={<ArrowDropDown />}
          sortIconBottom={<ArrowDropUp />}
          sortIconTopBot={<ArrowDropDown />}
          onSort={true}
          paginationProps={{
            totalCount: state.count,
            pageSize: state.pagination.pageSize,
            currentPage: state.pagination.currentPage,
            onPageNumberChange: (e) => {
              setState({
                ...state,
                pagination: { ...state.pagination, currentPage: e },
              });
            },
            onChangePageSize: (e) => {
              setState({
                ...state,
                pagination: { currentPage: 1, pageSize: e },
              });
            },
          }}
        />
      )}
    </div>
  );
}

export default IndividualUserList;
