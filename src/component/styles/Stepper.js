import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
  stepperContainer:{
      width:"100%",
      height:331,
      backgroundColor:"#F1F1F1",
      borderRadius: 1,
      boxShadow:"px 0px 10px #0000001A",
      opacity: 1,
     
  }
}));
