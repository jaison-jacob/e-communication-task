import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme)  => ({
    footerContainer:{
        width:"100%",
        height:64,
      background:"#FFFFFF 0% 0% no-repeat padding-box",
        boxShadow:"0px -5px 10px #0000000F",
        zIndex:9999,
        position:"fixed",
        bottom:20,
        opacity:1,
        
    },
    buttonContainer:{
        width:"30%",
        height:"100%",
        float:"right",
        
        marginRight:theme.spacing(20),
        display:"flex",
        alignItems:"center",
        justifyContent:"space-evenly"
    },
    saveBtn:{
        background:"#2BAA96 0% 0% no-repeat padding-box",
        borderRadius:2,
        opacity:1,
        fontFamily:"Rubik",
        fontSize:16,
        color:"#FFFFFF",
    },
    cancelBtn:{
      background:"#0000000A 0% 0% no-repeat padding-box",
      borderRadius:2,
      opacity:1,
      color:"#0000008A",
      fontFamily:"Rubik",
      fontSize:16,
      border:"none"
    }
  
  }));