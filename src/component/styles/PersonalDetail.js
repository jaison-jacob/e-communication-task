import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
    textField: {
        fontSize: 16,
        color: "#909090",
        fontFamily: "Rubik",
      },
      radioLabel: {
        color: "#000000DE",
        fontSize: 14,
        fontFamily: "Roboto",
        textAlign: "left",
        opacity: 1,
      },
      radioHead: {
        color: "#909090",
        fontSize: 16,
        fontFamily: "Rubik",
        textAlign: "left",
        marginBottom:theme.spacing(2)
      },
      selectLabel: {
        fontSize: 16,
        color: "#909090",
        fontFamily: "Rubik",
        fontWeight: 200,
      },
      selectOption: {
        fontSize: 16,
        color: "#909090",
        fontFamily: "Rubik",
      },
      chip: {
        backgroundColor: "#C1B8B8",
        borderRadius: 16,
        fontSize: 12,
        color: "#000000DE",
        fontFamily: "Roboto",
      },
      checkLabel: {
        color: "#000000DE",
        fontSize: 14,
        fontFamily: "Rubik",
        textAlign: "left",
        opacity: 1,
      },
      sendEmail:{
        background:"#0000000A 0% 0% no-repeat padding-box",
      borderRadius:2,
      opacity:1,
      color:"#0000008A",
      fontFamily:"Rubik",
      fontSize:16,
      border:"none",
      textTransform:"capitalize"
      }
}));
