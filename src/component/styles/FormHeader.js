import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
  formHeader: {
    height: 40,
    display: "flex",
    alignItems: "center",
    paddingLeft: theme.spacing(3),
    marginBottom: theme.spacing(2),
    backgroundColor: "#FFFFFF",
  },
  formHeaderText: {
    color: "#000000",
    fontFamily: "Rubik",
    fontSize: 20,
    marginLeft: theme.spacing(1),
    fontWeight: 900,
    
  },
  formHeaderIcon: {
    color: "#000000",
  },
}));
