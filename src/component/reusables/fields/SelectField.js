import React from "react";
import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormHelperText,
} from "@material-ui/core";
import {useField} from "formik"

function SelectField(props) {

  const {name,label,selectLabelStyle,options,selectOptionStyle,textStyle,...otherProps} = props
const [field,mata] = useField(name)
const configSelectField = {
  ...field,
  ...otherProps,
}

if(mata&& mata.touched && mata.error){
  configSelectField.error = true;
  configSelectField.helperText = mata.error;
}

  return (
    <div>
      <FormControl
        variant="filled"
        fullWidth
        error={Boolean(props.errorText)}
        disabled={props.disable}
      >
        <InputLabel className={selectLabelStyle}>
          {label}
        </InputLabel>
        <Select
          {...configSelectField}
          className={selectOptionStyle}
        >
          {/* change value e.name to e.id, change optiion to options */}
          {options.map((e, i) => (
            <MenuItem value={e.id} key={i}>
              {e.name}
            </MenuItem>
          ))}
        </Select>
        <FormHelperText>{props.errorText}</FormHelperText>
      </FormControl>
    </div>
  );
}

export {SelectField};
