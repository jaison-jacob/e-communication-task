import React from "react";
import { Chip, TextField } from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import {useFormikContext,useField} from "formik"

function AutoCompleteField(props) {

  const {name,options,style,chipVariant,textVariant,...otherProps} = props
  const {setFieldValue} = useFormikContext()

  const [field,mata] = useField(name)

  const handleChange = (e) => {
    setFieldValue(name,e)
  }

const configAutoCompleteField = {
  ...field,
  ...otherProps,
  
}

if(mata&& mata.touched && mata.error){
  configAutoCompleteField.error = true;
  configAutoCompleteField.helperText = mata.error;
}

  return (
    <div>
      <Autocomplete
        multiple
        id="tags-filled"
        options={options}
        getOptionLabel={(option) => option.name}
        onChange={(e, newValue, l) => {
        handleChange(newValue);
        }}
        
        renderTags={(value, getTagProps) =>
          value.map((option, index) => (
            <Chip
              key={index}
              classes={style}
              variant={chipVariant || "outlined"}
              label={option.name}
              {...getTagProps({ index })}
            />
          ))
        }
        renderInput={(params) => (
          <TextField
            {...params}
            variant={textVariant || "filled"}
            {...configAutoCompleteField}
          />
        )}
      />
    </div>
  );
}

export { AutoCompleteField };
