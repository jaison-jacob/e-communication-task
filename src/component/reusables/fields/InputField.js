import React from "react";
import { TextField } from "@material-ui/core";
import {useField} from "formik";

function InputField(props) {

const {name,textStyle,...otherProps} = props
const [field,mata] = useField(name)
const configTextField = {
  ...field,
  ...otherProps,
}

if(mata&& mata.touched && mata.error){
  configTextField.error = true;
  configTextField.helperText = mata.error;
}
  return (
    <div>
      <TextField
        id="filled-basic"
        {...configTextField}
        inputProps={{
          className: textStyle,
        }}
        // InputLabelProps={{
        //   shrink: props.shrink,
        // }}
        // fullWidth
        // error={Boolean(props.errorText)}
        // helperText={props.errorText}
      />
    </div>
  );
}

export {InputField};
