import React from "react";
import {
  FormControl,
  FormLabel,
  FormGroup,
  FormControlLabel,
  Checkbox,
  FormHelperText,
} from "@material-ui/core";
import {useFormikContext,useField} from "formik"
import {CheckBoxOutlineBlank} from '@material-ui/icons';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import {checkedState} from "../../../constants/induvidualUser"


function CheckBoxFeild(props) {

  const {name,checkBoxHead,boxColor,label,options,checkLabel} = props
  const {setFieldValue} = useFormikContext()

  const [field,mata] = useField(name)

  const handleChange = (e) => {
    let checkValue = field.value
    if(e.target.checked){
      checkValue.push(JSON.parse(e.target.value))
    }else{
      checkValue = checkValue.filter((item) => item.id!==JSON.parse(e.target.value).id)
    }
    setFieldValue(name,checkValue)
  }

const configAutoCompleteField = {
  ...field,
  
  onChange:handleChange
}

if(mata&& mata.touched && mata.error){
  configAutoCompleteField.error = true;
  configAutoCompleteField.helperText = mata.error;
}

  return (
    // change the hard coded label to dynamic props label,  change the value to e.id
    <div>
      <FormControl component="fieldset" {...configAutoCompleteField}>
        <FormLabel component="legend" focused={false} classes={checkBoxHead}>
          {label}
        </FormLabel>
        <FormGroup row>
          {options.map((e,index) => (
            <FormControlLabel
            key={index}
              control={
                <Checkbox
                  checked={checkedState(e.id,null,field.value)}
                  icon={<CheckBoxOutlineBlank style={boxColor}/>}
                   checkedIcon={<CheckBoxIcon  style={boxColor}/>}
                  size="small"
                  value={JSON.stringify(e)}
                  key={index}
                />
              
              }
              label={e.name}
              classes={checkLabel}
              
            />
          ))}
        </FormGroup>
      </FormControl>
    </div>
  );
}

export {CheckBoxFeild};
