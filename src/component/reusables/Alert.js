import React from 'react'
import {ButtonWrapper} from "../reusables"
import { Typography } from "@material-ui/core";

function Alert(props) {
    const {yesFun,noFun,alertMsg,conditionMsg,NoBtn,BtnText} = props
   
    return (
        <div>
            <div style={{padding:10}}>
    <Typography style={{fontSize:20,marginBottom:20}}>
        {alertMsg}
       </Typography>
    <Typography style={{fontSize:20,color:"red",marginBottom:20}}>
        {conditionMsg}
         </Typography>
    <div style={{display:"flex",float:"right"}}>
        {
            NoBtn && (
<ButtonWrapper
          text="NO"
          style={{marginLeft:20,backgroundColor:"green"}}
          size="large"
          variant="contained"
          BtnAction={noFun}
        />
            )
        }
    
    <ButtonWrapper
          text={BtnText}
          size="large"
          variant="contained"
          BtnAction={yesFun}
          style={{marginLeft:20,backgroundColor:"red"}}
        />
    </div>
    

  </div>
        </div>
    )
}

export {Alert}
