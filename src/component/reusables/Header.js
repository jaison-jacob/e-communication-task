import React, { useState } from "react";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import Avatar from "@material-ui/core/Avatar";
import { Box, Typography, Button } from "@material-ui/core";
import vaduvelu from "../../asserts/vadivelu-best-hd-photos-download-1080p-whatsapp-dpstatus-images-uksj-550x366.jpeg";
import { menuTitle, profileItem } from "../../constants/Header";
import clsx from "clsx";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import {useStyles} from "../styles/Header"
// import s from '../../asserts/Group 2034@2x.png'


 function Header(props) {
  const classes = useStyles();
  const history = useHistory();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [itemVisssible, setItemVissible] = useState({
    vissibleItem: [],
    status: null,
    key: null,
  });
  const [active, setActive] = useState(0);
  const open = Boolean(anchorEl);
  
  const handleMenu = (event, status, ind) => {
    
    if (status === "menuItemVissible") {
      if (menuTitle[ind].menuItem.length === 0) {
        history.push(menuTitle[ind].routePath);
      }
      setItemVissible({
        ...itemVisssible,
        vissibleItem: menuTitle[ind].menuItem,
        status: status,
        key: menuTitle[ind].key,
      });
    } else if (status === "profileItemVissible") {
      setItemVissible({
        ...itemVisssible,
        vissibleItem: profileItem,
        status: status,
      });
    }
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const changeActive = () => {
    setActive(itemVisssible.key);
    setAnchorEl(null);
  };

  return (
    <div className={classes.root}>
      <Toolbar
        className={classes.toolbar}
        classes={{ gutters: classes.toolbarGutters }}
      >
        <Box className={classes.title}>
          {menuTitle.map((title, index) => (
            <Button
              variant="text"
              className={clsx(classes.navlink, {
                [classes.navlinkActive]: history.location.pathname.includes(
                  title.key
                ),
              })}
              key={index}
              onClick={(e) => handleMenu(e, "menuItemVissible", index)}
            >
              {title.titleName}
            </Button>
          ))}
        </Box>

        <div
          className={classes.profileContainer}
          onClick={(e) => handleMenu(e, "profileItemVissible", null)}
        >
          <div>
            <Typography variant="subtitle1" className={classes.profileText}>
              Name
            </Typography>
            <Typography
              variant="inherit"
              className={classes.profileText}
              style={{ color: "#FFFFFF8A", fontSize: 12 }}
            >
              Role
            </Typography>
          </div>
          <IconButton
            aria-label="account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            color="inherit"
          >
            <Avatar alt="hjh" src={vaduvelu}></Avatar>
          </IconButton>
        </div>
        {itemVisssible.vissibleItem.length > 0 && (
          <Menu
            id="menu-appbar"
            anchorEl={anchorEl}
            getContentAnchorEl={null}
            keepMounted
            elevation={1}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "center",
            }}
            transformOrigin={{
              vertical: "top",
              horizontal: "center",
            }}
            open={open}
            onClose={handleClose}
            className={classes.profileMenu}
          >
            {itemVisssible.status === "profileItemVissible"
              ? itemVisssible.vissibleItem.map((item, index) => (
                  <MenuItem
                    onClick={handleClose}
                    key={index}
                    className={classes.menuItemText}
                  >
                    {item.profiletext}
                  </MenuItem>
                ))
              : itemVisssible.vissibleItem.map((item, index) => (
                  <Link
                    to={item.subMenuItemPath}
                    style={{ textDecoration: "none" }}
                  >
                    <MenuItem
                      onClick={() => changeActive()}
                      key={index}
                      className={classes.menuItemText}
                    >
                      {item.subMenuItemName}
                    </MenuItem>
                  </Link>
                ))}
          </Menu>
        )}
      </Toolbar>
    </div>
  );
}
export {Header}
