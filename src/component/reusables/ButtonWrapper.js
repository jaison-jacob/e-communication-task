import { Button } from '@material-ui/core'
import React from 'react'

function ButtonWrapper(props) {
    const {text,btnStyle,variant,size,disabled,BtnAction,...otherProps} = props

    return (
        <div>
            <Button className={btnStyle}  disabled={disabled} size={size}variant={variant}{...otherProps} onClick={BtnAction}>
                {text}
            </Button>
        </div>
    )
}

export {ButtonWrapper}
