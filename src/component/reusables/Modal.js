import React from "react";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import { DialogContent, Typography, IconButton } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import { useStyles } from "../styles/ModalStyle";
import {ButtonWrapper} from "../reusables"

  function Modal(props) {
  const classes = useStyles();
  const { children, setOpen, open } = props;

  return (
    <div>
      <Dialog
        onClose={() => setOpen(false)}
        aria-labelledby="simple-dialog-title"
        open={open}
        classes={{ paper: classes.dialogStyle }}
      >
        
        <DialogContent>{children}</DialogContent>


      </Dialog>
    </div>
  );
}

export {Modal}
