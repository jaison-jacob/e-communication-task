export const ecommunicationPath = {
    INDIVIDUALUSERFORM:"/",
    INDIVIDUALUSERLIST:"/table",
    COMPANYDETAILS:"/companyLicence/companyDetails",
    EMPLOYEEDETAILS:"/companyLicence/employeeDetails",
    INDIVIDUALPLAN:"/Plans/inidividualPlan",
    TEAMPLAN:"/plans/teamPlan",
    COMPANYPLAN:"/plans/companyPlan",
    NOTIFICATION:"/notification",
    PROMOTIONS:"/promotions",
    DASHBOARD:"/report/dashboard",
    PAYMENT:"/report/payment",
    USERHISTORY:"/report/userHistory",
    COMPANYHISTORY:"/report/companyHistory"
}