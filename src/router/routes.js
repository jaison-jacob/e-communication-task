import IndividualUserForm from "../pages/inidividualUser/IndividualUserForm";
import IndividualUserList from "../component/individualUser/individualUserList/IndividualUserList"

import {ecommunicationPath} from "./EcommunicationPath"

export const routes = [
	{
		path: ecommunicationPath.INDIVIDUALUSERFORM,
		component: IndividualUserForm,
		exact: true,
	},
	{
		path: ecommunicationPath.INDIVIDUALUSERLIST,
		component: IndividualUserList,
		exact: true,
	},
]