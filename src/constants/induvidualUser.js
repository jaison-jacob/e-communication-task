export const initialValue = {
  formControl: "personalDetails",
  personalDetails: {
    mailId: "",
    mobNo: "",
    name: "",
    age: "",
    genderId: 1,
    dateOfBirth: null,
    motherTongueId: "",
    preferredLanguageId: [],
    knownViaProducts: [],
    others: "",
  },
  addressDetails: {
    address: "",
    stateId: "",
    districtId: "",
    pincode: "",
    country: 1,
    type: "",
  },
  qualificationDetails: {
    userRoleId: 1,
    userQualificationId: "",
    institutionName: "",
    institutionAddress: "",
    country: 1,
    studyingAt: "",
    stateId: "",
    districtId: "",
    pincode: "",
    levelId: "",
    annumSal: "",
  },
  credentialDetails: {
    userName: "",
    generatePassword: "",
  },
};

export const checkedState = (id, name, field) => {
  for (let item = 0; item < field.length; item++) {
    if (Number(field[item].id) === id || field[item].name === name) {
      return true;
    }
  }
  return false;
};
export const submitValueOrdering = (data) => {
  console.log(data);
  let resultData = {};

  for (let item in data) {
    if (item === "personalDetails" || item === "addressDetails"|| item === "qualificationDetails") {
      let itemData = { ...data[item] };

      if (item === "personalDetails") {
        for (let subItem in itemData) {
          if (
            subItem === "preferredLanguageId" ||
            subItem === "knownViaProducts"
          ) {
            let res = itemData[subItem].map((e) => e.id);
            console.log(res);
            itemData = { ...itemData, [subItem]: res };
          }
        }
      }

      if (item === "addressDetails" || item === "qualificationDetails") {
        for (let subItem in itemData) {
          if (subItem === "country") {
            if (itemData[subItem] === 1) {
              itemData = { ...itemData, [subItem]: "India" };
            } else {
              itemData = { ...itemData, [subItem]: "Oversease" };
            }
          }
        }
      }

      console.log(itemData);

      resultData = { ...resultData, [item]: itemData };
    }
  }
  console.log(resultData);
  return resultData;
};
