import {ecommunicationPath} from "../router/EcommunicationPath"

export const menuTitle = [
	{
		titleName: "Individual License",
		menuItem: [
		],
		key: "Individual",
        routePath:"/"
	},
	{
		titleName: "Company Licence",
		menuItem: [
			{
				subMenuItemName: "Company Details",
				subMenuItemPath: ecommunicationPath.COMPANYDETAILS,
			},
			{
				subMenuItemName: "Employee Details",
				subMenuItemPath: ecommunicationPath.EMPLOYEEDETAILS,
			}
		],
		key: "companyLicence",
	},
	{
		titleName: "Plans",
		menuItem: [
			{
				subMenuItemName: "Individual Paln",
				subMenuItemPath: ecommunicationPath.INDIVIDUALPLAN,
			},
			{
				subMenuItemName: "Team Plan",
				subMenuItemPath: ecommunicationPath.TEAMPLAN,
			},
			{
				subMenuItemName: "Company Plan",
				subMenuItemPath:ecommunicationPath.COMPANYPLAN,
			},
		],
		key: "plans",
	},
	{
		titleName: "Notifications",
		menuItem: [
			
		],
		key: "notification",
        routePath:"/notification"
	},
	{
		titleName: "Promotions",
		menuItem: [
		],
		key: "promotions",
        routePath:"/promotions"
	},
	{
		titleName: "Report",
		menuItem: [
			{
				subMenuItemName: "Dashboard",
				subMenuItemPath: ecommunicationPath.DASHBOARD,
			},
			{
				subMenuItemName: "Payment",
				subMenuItemPath: ecommunicationPath.PAYMENT,
			},
            {
				subMenuItemName: "User History",
				subMenuItemPath: ecommunicationPath.USERHISTORY,
			},
            {
				subMenuItemName: "Company History",
				subMenuItemPath: ecommunicationPath.COMPANYHISTORY,
			},
		],
		key: "report",
	},
]

export const profileItem = [
	{
		profiletext: "Roles & Access",
	},
	{
		profiletext: "Our Employeees",
	},
	{
		profiletext: "Jockey Catagories",
	},
	{
		profiletext: "Prameters",
	},
	{
		profiletext: "Themes",
	},
	{
		profiletext: "Logout",
		active: true,
	},
]
