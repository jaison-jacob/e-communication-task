// import { Link } from 'react-router-dom';
// import {todo_Edit,todo_Delete} from '../../redux/action'
import { Avatar, Button, IconButton, Typography } from "@material-ui/core";
import SortIcon from "@material-ui/icons/Sort";
import { Box } from "@material-ui/core";
import { format } from "date-fns";

import colomnProduct from "../asserts/Ellipse 1.png"
// import DOTT from "./assets/Group 1794 (1).svg";
// import PrfilePopUp from "../../../component/notificationList/ProfilePopUp";
// import "../../../  /OrganizationTable.scss"
// import "../../../component/notificationList/style/OrganizationTable.scss";
// import ColumnHideCheckBox from "../../../component/notificationList/ColumnHideCheckBox";
// import classes from "*.module.css";

export const COLUMNS = () => [
  {
    Header: "product Name",
    accessor: "product",
    sticky: "left",
    width: 200,
    minWidth: 200,
    maxWidth: 450,
    Cell: (orgin) => {
     
      return (
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            color: "#000000DE",
            width: "100%",
          }}
        >
          <div style={{ display: "flex", alignItems: "center", width: "40%" }}>
            <Avatar src={colomnProduct} style={{ width: 32, height: 32 }} />
            <Typography
              component="h6"
              style={{ fontSize: 14, marginLeft: "50%" }}
            >
              alert
            </Typography>
          </div>  
          {/* <PrfilePopUp index={orgin.cell.row.original.id} /> */}
        </div>
      );
    },
  },

  
  {
    Header: "Name",
    accessor: "name",
    width: 250,
    minWidth: 200,
    maxWidth: 300,
    color: "#0000008A",
  },
  {
    Header: "Email",
    accessor: "mailId",
    width: 250,
    minWidth: 200,
    maxWidth: 300,
    color: "#000000B8",
  },
  {
    Header: "Mobile",
    accessor: "mob",
    width: 250,
    minWidth: 200,
    maxWidth: 300,
    color: "#0000008A",
  },
  {
    Header: "Address",
    accessor: "address",
    width: 250,
    minWidth: 200,
    maxWidth: 300,
    color: "green",
  },
  {
    Header: "State",
    accessor: "state",
    width: 250,
    minWidth: 200,
    maxWidth: 300,
    color: "#000000B8",
  },

  {
    Header: "District",
    accessor: "district",
    width: 250,
    minWidth: 200,
    maxWidth: 300,
    color: "#000000B8",
  },
  {
    Header: "User Role",
    accessor: "userRole",
    width: 250,
    minWidth: 200,
    maxWidth: 300,
    color: "#000000B8",
  },
  // {
  // 	Header: "Longitude",
  // 	accessor: "Longitude",
  // 	width: 250,
  // 	minWidth: 200,
  // 	maxWidth: 300,
  // 	color: "#000000B8",
  // },
  // {
  // 	Header: "LandMark",
  // 	accessor: "LandMark",
  // 	width: 250,
  // 	minWidth: 200,
  // 	maxWidth: 300,
  // 	color: "#000000B8",
  // },
  // {
  // 	Header: "Distance From LandMark",
  // 	accessor: "distanceFromLandMark",
  // 	width: 250,
  // 	minWidth: 200,
  // 	maxWidth: 300,
  // 	color: "#000000B8",
  // },
  // {
  // 	Header: "Nearest",
  // 	accessor: "Nearest",
  // 	width: 250,
  // 	minWidth: 200,
  // 	maxWidth: 300,
  // 	color: "#000000B8",
  // },
  // {
  // 	Header: "Effective Date",
  // 	accessor: "effectiveDate",
  // 	Cell: ({ value }) => {
  // 		return format(new Date(value), "dd/MM/yyyy")
  // 	},
  // 	width: 250,
  // 	minWidth: 200,
  // 	maxWidth: 300,
  // },
  // {
  // 	Header: "Reason For Deactive",
  // 	accessor: "reasonForDeactive",
  // 	width: 250,
  // 	minWidth: 200,
  // 	maxWidth: 300,
  // 	color: "#000000B8",
  // },
];
// {
  //   id: 2,
  //   Header: "Name",
  //   accessor: "name",
  //   Cell: ({ original }) => {
  //     console.log(original)
  //   },

    // getProps: () => ({}),
    // name: "name",

  // },


  export const COLUMN = () => [

    
      
    
    
      {
        Header: "id",
        accessor: "id",
        width: 250,
        minWidth: 200,
        maxWidth: 300,
        color: "#0000008A",
      },
      {
        Header: "name",
        accessor: "name",
        width: 250,
        minWidth: 200,
        maxWidth: 300,
        color: "#0000008A",
      }
      
    
  ]



