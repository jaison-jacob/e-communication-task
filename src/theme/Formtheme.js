import {
    
    createMuiTheme,
    
  } from "@material-ui/core";

export const formTheme = createMuiTheme({
    palette: {
      primary: {
        // Purple and green play nicely together.
        main: "#909090",
      },
      secondary: {
        // This is green.A700 as hex.
        main: "#2386A5DE",
        light: "#ff9800",
      },
      colors:{
          navbarcolor:"red"
      }
    },
  });