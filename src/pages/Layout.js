import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { routes } from "../router/routes";
import { Header } from "../component/reusables";
import { formTheme } from "../theme/Formtheme";
import { ThemeProvider } from "@material-ui/core";

function Layout() {
  console.log(routes);

  return (
    <ThemeProvider theme={formTheme}>
      <div>
        <Header />
        <Router>
          <Switch>
            {routes.map((item, index) => {
              return (
                <Route
                  key={index}
                  path={item.path}
                  component={item.component}
                  exact={item.exact}
                />
              );
            })}
          </Switch>
        </Router>
      </div>
    </ThemeProvider>
  );
}

export default Layout;
