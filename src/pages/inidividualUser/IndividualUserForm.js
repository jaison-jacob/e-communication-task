import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid } from "@material-ui/core";
import StepperDemo from "../../component/individualUser/Stepper";
import PersonalDetails from "../../component/individualUser/PersonelDetails";
import CommunicationAddress from "../../component/individualUser/CommunicationAddress";
import ProfessionalDetails from "../../component/individualUser/ProfessionalDetails";
import CredentialDetail from "../../component/individualUser/CredentialDetails"
import { FormWrapper, FormHeader,Modal } from "../../component/reusables";
import { Formik, Form } from "formik";
import { initialValue } from "../../constants/induvidualUser";
import FormFooter from "../../component/individualUser/FormFooter";
import {individualUserFormValidation} from "../../validation/IndividualUser";
import {submitValueOrdering} from "../../constants/induvidualUser";
import {postIndividualUser} from "../../api/Api"
// import classes from '*.module.css';

const useStyles = makeStyles((theme) => ({
  formContainer: {
    width: "80%",
    //   height:500,
    margin: `${theme.spacing(2)}px auto`,
  },
  product: {
    height: "64vh",
    overflowY: "auto",
    "&::-webkit-scrollbar": {
      display: "none",
    },
  },
  formHead:{
    marginTop:theme.spacing(2),
    marginLeft:theme.spacing(4)
  }
}));

function IndividualUserForm() {
  const styles = useStyles();
  const [activeStep, setActiveStep] = useState(0);
  const [skippedStep,setSkippedStep] = useState([]);
  const [completed, setCompleted] = useState(new Set());
  const [open, setOpen] = React.useState(false);
  const [modalComp, setModalComp] = useState();
  
  const steps = [
    { 
      id:1,
      component:<PersonalDetails/>,
      label:"Personal Details",
      formName:"personalDetails"
    },
    {
      id:2,
      component:<CommunicationAddress/>,
      label:"Address Details",
      formName:"addressDetails"
    },
    {
      id:3,
      component:<ProfessionalDetails/>,
      label:"Professional Details",
      formName:"qualificationDetails"
    },
    {
      id:4,
      component:<CredentialDetail/>,
      label:"Credential Details",
      formName:"credentialDetails"
    },
  ]

  const getComponent = () => {
    return steps[activeStep].component
  }
  
  const c = (value,reserForm) => {
    // console.log(e)

    

    completed.add(activeStep)

    if(activeStep !== steps.length-1){
      
      setActiveStep(activeStep + 1)
    }else{
     let result =  submitValueOrdering(value)
     console.log(result)
     postIndividualUser(result).then((res) => {
       console.log(res)
     }).catch((e) => {
       console.log(e)
     })
    }
    
   
  }
  

  

  return (
    <div>
      
      <div >
        <Grid container>
        <Grid item xs={1}></Grid>
          <Grid item xs={8} className={styles.formHead}>
            <FormHeader name="Individual Users" />
          </Grid>
        </Grid>
        <Grid container className={styles.product}>
        <Grid item xs={1}></Grid>
          <Grid item xs={4}>
            <StepperDemo 
            activeStep={activeStep}
            setActiveStep={setActiveStep}
            steps={steps}
            completed={completed}
            />
          </Grid>

          
            
              <Formik
                initialValues={{ ...initialValue }}
                onSubmit={(value,r) => {
                  console.log(value);
                  c(value,r.resetForm); 
                }}
                validationSchema={individualUserFormValidation}
              >
                <>
                <Grid item xs={6}>
                <Form>
                  {
                    getComponent()
                  }
                </Form>
                </Grid>
                <Grid item xs={12}>
          <FormFooter 
       activeStep={activeStep}
       skippedStep={skippedStep}
       completed={completed}
       setActiveStep={setActiveStep}
       setSkippedStep={setSkippedStep}
       setCompleted={setCompleted}
       steps={steps}
       open={open} 
       setOpen={setOpen}
       setModalComp={setModalComp}
      />
          </Grid>
                </>
              </Formik>
            
         
          
        </Grid>
        <Modal open={open} setOpen={setOpen}>
          {modalComp}
        </Modal>
      </div>
    </div>
  );
}

export default IndividualUserForm;
